Rails.application.routes.draw do
  get 'companies',to: 'companies#index'
  get 'companies/:id', to:'companies#show', as: :company
  #get 'home/index'

  root 'home#index'
  resources :phones
  resources :addresses
  resources :contacts
  resources :kinds, except: [:destroy]
  # resources :pets
  # resources :people
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
